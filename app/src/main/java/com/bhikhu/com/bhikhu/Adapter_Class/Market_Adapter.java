package com.bhikhu.com.bhikhu.Adapter_Class;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bhikhu.com.bhikhu.Model_Class.Market_Model;
import com.bhikhu.com.bhikhu.R;

import java.util.List;

public class Market_Adapter extends RecyclerView.Adapter<Market_Adapter.MyViewHolder> {

    private List<Market_Model> Market_ModelsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, lastprice, markettime;
        LinearLayout headerlinear;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            lastprice = view.findViewById(R.id.lastprice);
            markettime = view.findViewById(R.id.markettime);
            headerlinear = view.findViewById(R.id.headerlinear);
        }
    }


    public Market_Adapter(List<Market_Model> Market_ModelsList) {
        this.Market_ModelsList = Market_ModelsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.market_recyclerview, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Market_Model Market_Model = Market_ModelsList.get(position);
        holder.name.setText(Market_Model.getMarketname());
        holder.lastprice.setText(Market_Model.getLastprice());
        holder.markettime.setText(Market_Model.getMarkettime());
    }

    @Override
    public int getItemCount() {
        return Market_ModelsList.size();
    }
}