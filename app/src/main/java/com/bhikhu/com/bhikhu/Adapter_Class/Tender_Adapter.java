package com.bhikhu.com.bhikhu.Adapter_Class;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bhikhu.com.bhikhu.Model_Class.Tender_Model;
import com.bhikhu.com.bhikhu.Model_Class.Tender_Model;
import com.bhikhu.com.bhikhu.R;

import java.util.List;

public class Tender_Adapter extends RecyclerView.Adapter<Tender_Adapter.MyViewHolder> {

    private List<Tender_Model> Tender_ModelsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, lastprice, markettime;
        LinearLayout headerlinear;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            lastprice = view.findViewById(R.id.lastprice);
            markettime = view.findViewById(R.id.markettime);
            headerlinear = view.findViewById(R.id.headerlinear);
        }
    }


    public Tender_Adapter(List<Tender_Model> Tender_ModelsList) {
        this.Tender_ModelsList = Tender_ModelsList;
    }

    @Override
    public Tender_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tender_recyclerview, parent, false);

        return new Tender_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(Tender_Adapter.MyViewHolder holder, int position) {
        Tender_Model Tender_Model = Tender_ModelsList.get(position);
        holder.name.setText(Tender_Model.getTenderName());
        holder.lastprice.setText(Tender_Model.getTenderDate());
        holder.markettime.setText(Tender_Model.getTenderDescription());
    }

    @Override
    public int getItemCount() {
        return Tender_ModelsList.size();
    }
}
