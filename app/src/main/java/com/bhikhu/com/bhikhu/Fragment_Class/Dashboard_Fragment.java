package com.bhikhu.com.bhikhu.Fragment_Class;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bhikhu.com.bhikhu.Common_Class.Common_Class;
import com.bhikhu.com.bhikhu.Common_Class.Shared_Common_Pref;
import com.bhikhu.com.bhikhu.R;

import static com.bhikhu.com.bhikhu.MainActivity.tool_Rel;


public class Dashboard_Fragment extends Fragment implements View.OnClickListener {
    View v;
    Common_Class common_class;
    Shared_Common_Pref shared_common_pref;
    TextView market, tender, event;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_dashboard, container, false);

        Declaration_View(v);

        return v;
    }

    private void Declaration_View(View v) {
        market = v.findViewById(R.id.market);
        tender = v.findViewById(R.id.tender);
        event = v.findViewById(R.id.event);
        tender.setOnClickListener(this);
        market.setOnClickListener(this);
        event.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.market:
                tool_Rel.setVisibility(View.GONE);
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.homepage, new Market_Fragment(), "Market");
                ft.commit();

                break;

            case R.id.tender:
                tool_Rel.setVisibility(View.GONE);
                final FragmentTransaction ftt = getFragmentManager().beginTransaction();
                ftt.replace(R.id.homepage, new Tender_Fragment(), "Tender");
                ftt.commit();


                break;
            case R.id.event:
                tool_Rel.setVisibility(View.GONE);
                final FragmentTransaction fte = getFragmentManager().beginTransaction();
                fte.replace(R.id.homepage, new Event_Fragment(), "Event");
                fte.commit();
                break;
        }
    }
}
