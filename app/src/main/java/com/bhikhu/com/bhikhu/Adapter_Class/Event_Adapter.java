package com.bhikhu.com.bhikhu.Adapter_Class;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bhikhu.com.bhikhu.Model_Class.Event_Model;
import com.bhikhu.com.bhikhu.Model_Class.Event_Model;
import com.bhikhu.com.bhikhu.R;

import java.util.List;

public class Event_Adapter extends RecyclerView.Adapter<Event_Adapter.MyViewHolder> {

    private List<Event_Model> Event_ModelsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, lastprice, markettime;
        LinearLayout headerlinear;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            lastprice = view.findViewById(R.id.lastprice);
            markettime = view.findViewById(R.id.markettime);
            headerlinear = view.findViewById(R.id.headerlinear);
        }
    }


    public Event_Adapter(List<Event_Model> Event_ModelsList) {
        this.Event_ModelsList = Event_ModelsList;
    }

    @Override
    public Event_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_recyclerview, parent, false);

        return new Event_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(Event_Adapter.MyViewHolder holder, int position) {
        Event_Model Event_Model = Event_ModelsList.get(position);
        holder.name.setText(Event_Model.getEventName());
        holder.lastprice.setText(Event_Model.getEventDate());
        holder.markettime.setText(Event_Model.getEventDescription());
    }

    @Override
    public int getItemCount() {
        return Event_ModelsList.size();
    }
}
