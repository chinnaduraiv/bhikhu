package com.bhikhu.com.bhikhu.Common_Class;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 *
 */

public class Shared_Common_Pref {
    SharedPreferences Common_pref;
    SharedPreferences.Editor editor;
    Activity activity;
    Context _context;

    public static final String username = "vehicle_pref";
    public static final String loggedIn = "loggedIn";
    public Shared_Common_Pref(Activity Ac) {
        activity = Ac;
        if (activity != null) {
            Common_pref = activity.getSharedPreferences("Preference_values", Context.MODE_PRIVATE);
            editor = Common_pref.edit();
        }
    }

    public Shared_Common_Pref(Context cc) {
        this._context = cc;
        Common_pref = cc.getSharedPreferences("Preference_values", Context.MODE_PRIVATE);
        editor = Common_pref.edit();
    }

    public void save(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public String getvalue(String key) {
        String text = null;
        text = Common_pref.getString(key, null);
        return text;
    }

    public void save_Longvalue(String key, Long value) {
        editor.putLong(key, value);
        editor.commit();
    }

    public Long getLong_value(String key) {
        Long text = null;
        text = Common_pref.getLong(key, 0);
        return text;
    }

    public void clear_pref(String key) {
        Common_pref.edit().remove(key).apply();


    }

}
