package com.bhikhu.com.bhikhu.Model_Class;

public class Tender_Model {
    public Tender_Model(String tenderName, String tenderDate, String tenderDescription) {
        TenderName = tenderName;
        TenderDate = tenderDate;
        TenderDescription = tenderDescription;
    }

    public String getTenderName() {

        return TenderName;
    }

    public void setTenderName(String tenderName) {
        TenderName = tenderName;
    }

    public String getTenderDate() {
        return TenderDate;
    }

    public void setTenderDate(String tenderDate) {
        TenderDate = tenderDate;
    }

    public String getTenderDescription() {
        return TenderDescription;
    }

    public void setTenderDescription(String tenderDescription) {
        TenderDescription = tenderDescription;
    }

    private String TenderName, TenderDate, TenderDescription;

}
