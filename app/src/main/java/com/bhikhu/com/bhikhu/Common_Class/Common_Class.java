package com.bhikhu.com.bhikhu.Common_Class;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.sdsmdg.tastytoast.TastyToast;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 */

public class Common_Class {


    Dialog dialog_invitation = null;
    Intent intent;
    Activity activity;
    public Context context;
    Shared_Common_Pref shared_common_pref;
    Gson gson;
    public static String value_true = null;
    Typeface typeface_regular, typeface_light, typeface_bold, typeface_medium;
    SharedPreferences addvehiclesharedprefrence;





    public void CommonIntentwithFinish(Class classname) {
        intent = new Intent(activity, classname);
        activity.startActivity(intent);
        activity.finish();
    }

    public void CommonIntentwithoutFinish(Class classname) {
        intent = new Intent(activity, classname);
        activity.startActivity(intent);

    }


    public void CommonIntentwithNEwTask(Class classname) {
        intent = new Intent(activity, classname);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    public void CommonIntentwithoutFinishputextra(Class classname, String title) {
        intent = new Intent(activity, classname);

        intent.putExtra("title", title);

        activity.startActivity(intent);

    }

    public void CommonIntentwithFinishputextra(Class classname, String title) {
        intent = new Intent(activity, classname);

        intent.putExtra("UserID", title);
        Log.e("commanclasstitle", title);
        activity.startActivity(intent);
    }


    public void commonSnackBar(String s, View act) {
        Snackbar snackbar = Snackbar.make(act, s, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    public void showToastMSG(Activity Ac, String MSg, int s) {

        TastyToast.makeText(Ac, MSg,
                TastyToast.LENGTH_LONG, s).setGravity(Gravity.CENTER, 0, 0);


    }

    public Common_Class(Context context) {
        this.context = context;
        addvehiclesharedprefrence = context.getSharedPreferences("logingetaccesstoken", MODE_PRIVATE);
        shared_common_pref = new Shared_Common_Pref(context);

    }

    public boolean isValidEmailId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    public Common_Class(Activity activity) {
        this.activity = activity;
        addvehiclesharedprefrence = activity.getSharedPreferences("logingetaccesstoken", MODE_PRIVATE);
        shared_common_pref = new Shared_Common_Pref(activity);


    }

    public void hidekeyboard(Activity activity) {
        this.activity = activity;
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public void Showkeyboard(Activity activity) {
        this.activity = activity;
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    public Typeface setFontStyle(String style) {
        Typeface result = null;
        if (style.equals("light")) {
            result = typeface_light;
        } else if (style.equals("bold")) {
            result = typeface_bold;
        } else if (style.equals("regular")) {
            result = typeface_regular;
        } else if (style.equals("medium")) {
            result = typeface_medium;
        }
        return result;
    }

    public boolean isNetworkAvailable(final Context context) {
        this.context = context;

        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();

            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }



    public void sharedialog(Context context) {

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "Here is the share content body";
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
        this.context = context;
    }

    public void sharedialogwithString(Context context, String msg) {

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = msg;
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
        this.context = context;
    }








    public EditText seterror(final View v, int a, String enter_company_name) {


        EditText e = (EditText) v.findViewById(a);
        e.requestFocus();
        e.setError(enter_company_name);
        return e;
    }







}
