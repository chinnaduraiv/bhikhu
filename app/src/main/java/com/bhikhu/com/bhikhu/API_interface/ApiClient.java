package com.bhikhu.com.bhikhu.API_interface;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {

    //public static final String BASE_URL = "http://www.fmcg.sanfmcg.com/server/";
    public static final String BASE_URL = "https://batsroost.com/webadmin/api/";
    private static Retrofit retrofit = null;
    //http://sanffa.info/iOSServer/db_det.php?axn=get/catvst

    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
