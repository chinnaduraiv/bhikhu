package com.bhikhu.com.bhikhu.Model_Class;

public class Event_Model {
    private String EventName, EventDate, EventDescription;

    public Event_Model(String eventName, String eventDate, String eventDescription) {
        EventName = eventName;
        EventDate = eventDate;
        EventDescription = eventDescription;
    }

    public String getEventName() {

        return EventName;
    }

    public void setEventName(String eventName) {
        EventName = eventName;
    }

    public String getEventDate() {
        return EventDate;
    }

    public void setEventDate(String eventDate) {
        EventDate = eventDate;
    }

    public String getEventDescription() {
        return EventDescription;
    }

    public void setEventDescription(String eventDescription) {
        EventDescription = eventDescription;
    }
}
