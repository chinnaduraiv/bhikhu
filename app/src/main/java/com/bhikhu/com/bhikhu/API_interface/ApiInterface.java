package com.bhikhu.com.bhikhu.API_interface;



import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


public interface ApiInterface {
    //REgister API
    @Headers({
            "Content-type:application/x-www-form-urlencoded",
            "Accept:application/json"
    })

    @FormUrlEncoded
    @POST("logistics.php?axn=addnewcustomer")
    Call<ResponseBody> AddcompanyDetails(
            @FieldMap Map<String, String> params
    );

    @FormUrlEncoded
    @POST("login.php?")
    Call<ResponseBody> Login(
            @FieldMap Map<String, String> params
    );
    @FormUrlEncoded
    @POST("signup.php?")
    Call<ResponseBody> Register(
            @FieldMap Map<String, String> params
    );

}
