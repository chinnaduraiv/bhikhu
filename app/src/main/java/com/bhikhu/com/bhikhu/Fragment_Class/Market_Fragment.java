package com.bhikhu.com.bhikhu.Fragment_Class;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhikhu.com.bhikhu.Adapter_Class.Market_Adapter;
import com.bhikhu.com.bhikhu.Common_Class.Common_Class;
import com.bhikhu.com.bhikhu.Common_Class.Shared_Common_Pref;
import com.bhikhu.com.bhikhu.Model_Class.Market_Model;
import com.bhikhu.com.bhikhu.R;
import com.bhikhu.com.bhikhu.View_Page_Adapter.SlideVIewpae_Adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Market_Fragment extends Fragment implements View.OnClickListener {
    View v;
    Common_Class common_class;
    Shared_Common_Pref shared_common_pref;
    TextView market, tender, event;
    public List<Market_Model> Market_ModelList = new ArrayList<>();
    private RecyclerView notificationrecyclerview;
    private Market_Adapter mAdapter;
    ImageView tool_backarrow;
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static final Integer[] XMEN = {R.drawable.slideone, R.drawable.slidetwo, R.drawable.slidethreejpg};
    private ArrayList<Integer> XMENArray = new ArrayList<Integer>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.market_fragment, container, false);
        preparenotificationData();
        mAdapter = new Market_Adapter(Market_ModelList);
        init();
        Declaration_View(v);
        return v;
    }

    private void Declaration_View(View v) {

        tool_backarrow = v.findViewById(R.id.back_arrow);
        notificationrecyclerview = v.findViewById(R.id.seededrecyclerview);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        notificationrecyclerview.setLayoutManager(mLayoutManager);
        notificationrecyclerview.setItemAnimator(new DefaultItemAnimator());
        notificationrecyclerview.setAdapter(mAdapter);
        tool_backarrow.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


        }
    }

    private void init() {
        for (int i = 0; i < XMEN.length; i++)
            XMENArray.add(XMEN[i]);
        mPager = (ViewPager) v.findViewById(R.id.pager);

        mPager.setAdapter(new SlideVIewpae_Adapter(getActivity(), XMENArray));


        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == XMEN.length) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 2500, 2500);
    }

    private void preparenotificationData() {


        Market_ModelList.add(new Market_Model("GOLD", "457.98", "10.30am"));

        Market_ModelList.add(new Market_Model("Steel", "45667.87", "12.pm"));

        Market_ModelList.add(new Market_Model("Petrol", "7878876.988", "4.50pm"));

        Market_ModelList.add(new Market_Model("Sugar", "567345", "9.30am"));


    }
}
