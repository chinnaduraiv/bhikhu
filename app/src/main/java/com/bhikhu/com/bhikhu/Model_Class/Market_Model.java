package com.bhikhu.com.bhikhu.Model_Class;

public class Market_Model {

    private String Marketname, Lastprice, Markettime;
    public Market_Model(String Marketname,String Lastprice,String Markettime ) {

        this.Marketname=Marketname;
        this.Lastprice=Lastprice;
        this.Markettime=Markettime;
    }

    public String getMarketname() {
        return Marketname;
    }

    public void setMarketname(String marketname) {
        Marketname = marketname;
    }

    public String getLastprice() {
        return Lastprice;
    }

    public void setLastprice(String lastprice) {
        Lastprice = lastprice;
    }

    public String getMarkettime() {
        return Markettime;
    }

    public void setMarkettime(String markettime) {
        Markettime = markettime;
    }



}
