package com.bhikhu.com.bhikhu.Activity_Class;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bhikhu.com.bhikhu.API_interface.ApiClient;
import com.bhikhu.com.bhikhu.API_interface.ApiInterface;
import com.bhikhu.com.bhikhu.Common_Class.Common_Class;
import com.bhikhu.com.bhikhu.Common_Class.Shared_Common_Pref;
import com.bhikhu.com.bhikhu.MainActivity;
import com.bhikhu.com.bhikhu.R;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import butterknife.BindView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register_Activity extends AppCompatActivity implements View.OnClickListener {
    ProgressBar progressBar;
    EditText Username, userEmail, userPassword, PhoneNo;
    Common_Class common_class;
    Button register;
    CheckBox checkBox1;
    ApiInterface apiService;
    Gson gson;
    Shared_Common_Pref sharedCommonPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        common_class = new Common_Class(Register_Activity.this);
        sharedCommonPref = new Shared_Common_Pref(Register_Activity.this);
        register = findViewById(R.id.register);
        userEmail = findViewById(R.id.et_mailid);
        userPassword = findViewById(R.id.et_password);
        Username = findViewById(R.id.et_name);
        PhoneNo = findViewById(R.id.et_userphone);
        checkBox1 = findViewById(R.id.checkBox1);
        progressBar = findViewById(R.id.progress_bar);
        gson = new Gson();
        register.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register:

                apiService = ApiClient.getClient().create(ApiInterface.class);
                if (common_class.isNetworkAvailable(Register_Activity.this)) {


                    if (vali()) {
                        progressBar.setVisibility(View.VISIBLE);


                        apiService = ApiClient.getClient().create(ApiInterface.class);
                        if (common_class.isNetworkAvailable(Register_Activity.this)) {
                            progressBar.setVisibility(View.VISIBLE);


                            HashMap<String, String> map = new HashMap<String, String>();


                            map.put("mail_id", userEmail.getText().toString());
                            map.put("country_code", "+91");
                            map.put("phone", PhoneNo.getText().toString());
                            map.put("password", userPassword.getText().toString());
                            System.out.println("Params" + map.toString());
                            Call<ResponseBody> Callto = apiService.Register(map);
                            Callto.enqueue(CheckUser);


                        } else {

                            common_class.showToastMSG(Register_Activity.this, getString(R.string.nointernetcnnection), 3);
                        }

                    }
                } else {

                    common_class.showToastMSG(Register_Activity.this, getString(R.string.nointernetcnnection), 3);
                }


                break;

        }
    }

    public boolean vali() {

        String emailPattern = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})";
        if (Username.getText().toString().equalsIgnoreCase("")) {
            Username.setError("Enter the Username ");

            Username.requestFocus();

            return false;
        }


        if (userEmail.getText().toString().equalsIgnoreCase("")) {
            userEmail.setError("Enter the Email ");

            userEmail.requestFocus();

            return false;
        }


        if (!userEmail.getText().toString().matches(emailPattern)) {

            userEmail.setError("Enter the Valid Email Address");

            userEmail.requestFocus();
            return false;
        }

        if (PhoneNo.getText().toString().equalsIgnoreCase("")) {
            PhoneNo.setError("Enter the mobile number");

            PhoneNo.requestFocus();

            return false;
        }

        if (PhoneNo.getText().toString().length() != 10) {
            PhoneNo.setError("Enter the correct mobile number");

            PhoneNo.requestFocus();

            return false;
        }
        if (userPassword.getText().toString().equalsIgnoreCase("")) {
            userPassword.setError("Enter the password");

            userPassword.requestFocus();

            return false;
        }

        if (!checkBox1.isChecked()) {

            common_class.showToastMSG(Register_Activity.this, "Accept Terms And Conditions?", 4);
            return false;
        }
        return true;
    }

    public Callback<ResponseBody> CheckUser = new Callback<ResponseBody>() {
        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

            System.out.println("checkUser is sucessful :" + response.isSuccessful());
            if (response.isSuccessful()) {
                JSONObject jsonObject = null;
                String jsonData = null;
                try {
                    jsonData = response.body().string();
                    jsonObject = new JSONObject(jsonData);
                    JSONArray jArray1 = jsonObject.getJSONArray("Details");

                    JSONObject object3 = jArray1.getJSONObject(0);
                    System.out.println("OUT_PUT" + object3);
                    if (object3.getString("response").equals("true")) {
                        progressBar.setVisibility(View.GONE);

                        common_class.showToastMSG(Register_Activity.this, object3.getString("msg"), 1);
                        sharedCommonPref.save(Shared_Common_Pref.loggedIn, "loggedIn");
                        common_class.CommonIntentwithNEwTask(Login_Activity.class);
                    } else {
                        progressBar.setVisibility(View.GONE);
                        common_class.showToastMSG(Register_Activity.this, "Something Went to wrong!", 3);
                    }
                    Log.d("TAG", "jsonObject: " + jsonObject);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    Toast.makeText(getApplicationContext(), jObjError.getString("error_msg"), Toast.LENGTH_LONG).show();
                    progressBar.setVisibility(View.GONE);

                    System.out.println("this is responsebody error" + jObjError.getString("success"));
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "catchbody error " + e.toString(), Toast.LENGTH_LONG).show();
                    System.out.println("catchbody error " + e.toString());
                }
            }
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }


    };
}
